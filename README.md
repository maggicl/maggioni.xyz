<!-- vim: set et sw=2 ts=2 tw=80 : -->

# maggioni.xyz

Jekyll source of website https://maggioni.xyz.

## Licensing

Content of articles is licensed under
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). HTML code is
licensed under the MIT license.
