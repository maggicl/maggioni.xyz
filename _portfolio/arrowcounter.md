---
layout: portfolio
title: Arrowcounter
date-start: 2018-07
date-end: now
category: personal
languages: Python (Django)
images:
  - arrowcounter/home.png
  - arrowcounter/counts.png
  - arrowcounter/counter.png
  - arrowcounter/stats.png
description: Simple AJAX counter with some backend storage and statistics, intended to count the number of arrows shot in an archery training session.
repo-type: git
repo-url: https://tea.maggioni.xyz/maggicl/arrowcounter
---

This website is a counter web application I developed in my spare time using the
python framework Django. The application is intended to be used as a counter for
arrows shot in an archery training session.

The application features a counter interface with multi-increment counter
buttons, a basic trend analyzer that measures the difference between the total
number of arrows shot and a yearly goal, CSV export capabilities,
and a statistics page.

The frontend was built with plain HTML/CSS/Javascript, aside the use of the
Charts.js api and Materialize as a CSS library.
