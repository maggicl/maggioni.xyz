---
layout: portfolio
title: Dr Brainf-ck
date-start: 2018-11
date-end: 2018-12
category: usi
languages: Racket
authors:
  - Tommaso Rodolfo Masera
images:
  - drbf/gui.png
  - drbf/doc.png
  - drbf/gui_diagram.png
description: Brainf-ck interpreter with integrated editor and stepper
repo-type: gitlab
repo-url: https://gitlab.com/maggicl/DrBrainf-ck
---

This application was built as the final project of _Programming Fundamentals 1_
class in USI. This was a pair project done with my colleague Tommaso Rodolfo
Masera.

This is a fully operational [Brainf-ck](https://en.wikipedia.org/wiki/Brainfuck)
interpreter, complete with a GUI editor interface and some debugging tools, such
as a memory tape inspector and a stepper.

The application was built using Racket, a Scheme dialect in the Lisp family of
programming languages. The interpreter portion of the program is fully
functional and lacks explicit mutability of variables.
