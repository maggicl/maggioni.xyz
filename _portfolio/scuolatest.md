---
layout: portfolio
title: Scuolatest
date-start: 2017-09
date-end: 2017-12
category: high-school
languages: PHP (Symfony 3) + HTML/CSS/JS
images:
  - scuolatest/benvenuto.png
  - scuolatest/test-list.jpg
  - scuolatest/test-matematica.png
  - scuolatest/esito-matematica.png
  - scuolatest/stats-matematica.png
authors:
  - Davide Fiori
  - Giorgio Croci
repo-type: gitlab
repo-url: https://gitlab.com/staccastacca/scuolatest
description: Self assessment quiz platform with integrated statistical review.
---

This project is a self-assessment tool intended for middle school students that wished to enroll in my high school. The website could be used by students (to take the test) and teachers (to review aggregated results in the tests, in order to tailor their curriculum.

The project was developed using Symfony 3, JQuery and ChartJS. The CSS library used is Materialize.

Credits to Davide Fiori and Giorgio Croci which helped me in building this application.
