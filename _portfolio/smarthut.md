---
layout: portfolio
title: Smarthut
date-start: 2020-03
date-end: 2020-06
category: usi
languages: Spring + ReactJS
authors:
  - Andrea Brites Marto
  - Matteo Omenetti
  - Jacob Salvi
  - Tommaso Rodolfo Masera
  - Nicola Brunner
  - Filippo Cesana
  - Christian Capeáns Pérez
images:
  - smarthut/home-frontend.png
  - smarthut/devices.png
  - smarthut/scenes.png
  - smarthut/automations.png
  - smarthut/swagger.png
description: Smart home manager for (mocked) smart devices, complete with user-definable rooms, presets, and events.
repo-type: git
repo-url: https://tea.maggioni.xyz/smarthut
---

This is a 3 month software engineering project part of the second year of USI's curriculum. As a team leader, I had to manage a team of 8 people to develop Smarthut, a full-stack Spring + ReactJS web application that manages smart devices.

Smarthut is able to direcly control (mocked) smart devices, but it can also group devices in rooms, configure and apply pre-made settings to a group of devices, and trigger user-defined actions based on a user-defined condition on pre-selected devices.

Smarthut's backend is a Spring REST application, complete with JWT sessions, CRUD APIs for the different types of device, and a WebSocket API that propagates device status updates. The frontend is a ReactJS application with Redux storage to handle active and passive device updates.
