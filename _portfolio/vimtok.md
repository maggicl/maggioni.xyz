---
layout: portfolio
title: Vimtok
date-start: 2019-11
date-end: 2019-12
category: usi
languages: ExpressJS (Node.js) + Mostly vanilla JS
authors:
  - Mejrima Smajilbasic
  - Renato Iannace
  - Matteo Omenetti
images:
  - vimtok/home.png
  - vimtok/gallery.png
  - vimtok/post.png
  - vimtok/user.png
description: Video snippet sharing social media website prototype
repo-type: github
repo-url: https://github.com/Carrisiland/vimtok
---

This website is a 4-week project for the _Software Atelier 3_ class in USI. This
was a group project of 4 people, in which I took turn at being the team leader.

The function of this website is to allow sharing parts of videos already
uploaded on _Youtube_ or _Vimeo_ without _freebooting_ the original content
creator. The user can specify a video link and a start-end time, and publish a
post on the platform. The post will appear as an either _Youtube_ or _Vimeo_
embedded video player looping automatically in the specified interval. Users can
also like and comment on posts, create playlists/albums of posts, and follow other
users and get notified of their uploads.

The website also features a special GIF generation feature implemented through
`youtube-dl` and `ffmpeg`.

The website is currently online [here](https://vimtok.maggioni.xyz), but signup
and creation of anonymous posts are disabled due to the unmoderated nature of
the website.
